<?php
define('PRIVAT24_DEBUG_ERRORS', true);

function commerce_privat24_prepayform(&$state){
    $form=array(); $currencies_ar=array();
    
    $form['currency'] = array(
		'#type' => 'radios',
		'#title' => t("Currency"),
		'#options' => $currencies_ar,
		'#default_value' => 'UAH',
		//'#description' => t("Select payment currency."),
		'#required' => TRUE
	);
    $form['amount'] = array(
		'#type' => 'textfield', 
		'#title' => t('Amount'), 
		'#default_value' => '', 
		'#size' => 10, 
		'#maxlength' => 12, 
		'#required' => TRUE
	);

    $form['memo'] = array(
            '#type' => 'textarea', 
            '#title' => t('Memo'), 
            '#default_value' => t('Payment to !sitename', array('!sitename' => variable_get('site_name', 'Drupal'))), 
            '#description' => t("Payment description."),
            '#required' => TRUE
    );


    $form['submit'] = array(
            '#type' => 'submit',
            '#value' => t('Cerate payment'),
    );

    return $form;
}

// Icon
function commerce_privat24_icon() {    
    $variable = array(
    'path' => drupal_get_path('module', 'commerce_privat24') . '/images/logo_big.png',
    );
return $variable;
    
} 
// Curriences
function commerce_privat24_currencies() {
  return drupal_map_assoc(array('EUR', 'USD', 'UAH'));
}